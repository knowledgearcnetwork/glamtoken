pragma solidity 0.5.0;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/ARCH.sol";

contract TestARCH {

    function testInitialBalance() public {
        ARCH ARCH = ARCH(DeployedAddresses.ARCH());

        uint256 expected = 1025000000 * (10 ** uint256(18));

        Assert.equal(ARCH.balanceOf(msg.sender), expected, "Owner should have 1025000000000000000000000000 ARCH Tokens initially!!!");
    }

    function testName() public {
        ARCH ARCH = new ARCH();

        string memory expected = "Archive";

        Assert.equal(ARCH.name(), expected, "Token name should be Archive.");
    }

    function testSymbol() public {
        ARCH ARCH = new ARCH();

        string memory expected = "ARCH";

        Assert.equal(ARCH.symbol(), expected, "Token symbol should be ARCH.");
    }

    function testDecimals() public {
        ARCH ARCH = new ARCH();

        uint256 expected = 18;

        Assert.equal(ARCH.decimals(), expected, "Token decimals should be 18.");
    }

     event ConsoleLog(address message);
}
