const { BN, constants, expectEvent, shouldFail } = require('openzeppelin-test-helpers');
const { ZERO_ADDRESS } = constants;

var ARCH = artifacts.require("./ARCH.sol");
var tokenAddress;

contract('ARCH', accounts => {
    const decimals = new BN('18');
    const initialSupply = new BN('1025000000').mul(new BN('10').pow(decimals));

    const initialHolder = accounts[0];
    const recipient = accounts[1];
    const anotherAccount = accounts[2];

	beforeEach(async function() {
		this.token = await ARCH.new()
	})

    describe('instantiation', function () {
        describe('totalSupply', function () {
        	it('initiates a total supply of 1 billion, 25 million ARCH tokens.', async function() {
        		(await this.token.totalSupply()).should.be.bignumber.equal(initialSupply);
        	})

            it('transfers the initial supply to the owner', async function() {
                (await this.token.balanceOf.call(initialHolder)).should.be.bignumber.equal(initialSupply);
            })
        });

        describe('balanceOf', function () {
            describe('when the requested account has no tokens', function () {
                it('returns zero', async function () {
                    (await this.token.balanceOf(anotherAccount)).should.be.bignumber.equal('0');
                });
            });
        });
    });

    describe('transfer', function() {
        const to = recipient;

        describe('for an existing recipient', function() {
            describe('when the sender has enough balance', function() {
                const amount = initialSupply;

                it('transfers ARCH to another account.', async function() {
                    await this.token.transfer(to, amount);

                    (await this.token.balanceOf(initialHolder)).should.be.bignumber.equal('0');

                    (await this.token.balanceOf(to)).should.be.bignumber.equal(amount);
                })

                it('emits a transfer event', async function () {
                    const { logs } = await this.token.transfer(to, amount);

                    expectEvent.inLogs(logs, 'Transfer', {
                        from: initialHolder,
                        to: to,
                        value: amount,
                    });
                });
            })

            describe('when the sender does not have enough balance', function() {
                const amount = initialSupply.addn(1);

                it('reverts', async function () {
                    await shouldFail.reverting(this.token.transfer(to, amount));
                });
            });
        })

        describe('when the recipient is the zero address', function () {
            const to = ZERO_ADDRESS;

            it('reverts', async function () {
                await shouldFail.reverting(this.token.transfer(to, initialSupply, { from: initialHolder }));
            });
        });

        describe('when the recipient is the token contract', function () {
            const to = ZERO_ADDRESS;

            it('reverts', async function () {
                await shouldFail.reverting(this.token.transfer(this.token.address, initialSupply, { from: initialHolder }));
            });
        });
    })

    describe('approve', function () {
        describe('when the spender is not the zero address', function () {
            const spender = recipient;

            describe('when the sender has enough balance', function () {
                const amount = initialSupply;

                it('emits an approval event', async function () {
                    const { logs } = await this.token.approve(spender, amount);

                    expectEvent.inLogs(logs, 'Approval', {
                        owner: initialHolder,
                        spender: spender,
                        value: amount,
                    });
                });

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        await this.token.approve(spender, amount);

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount);
                    });
                });

                describe('when the spender had an approved amount', function () {
                    beforeEach(async function () {
                        await this.token.approve(spender, new BN(1));
                    });

                    it('approves the requested amount and replaces the previous one', async function () {
                        await this.token.approve(spender, amount);

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount);
                    });
                });
            });

            describe('when the sender does not have enough balance', function () {
                const amount = initialSupply.addn(1);

                it('emits an approval event', async function () {
                    const { logs } = await this.token.approve(spender, amount);

                    expectEvent.inLogs(logs, 'Approval', {
                        owner: initialHolder,
                        spender: spender,
                        value: amount,
                    });
                });

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        await this.token.approve(spender, amount);

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount);
                    });
                });

                describe('when the spender had an approved amount', function () {
                    beforeEach(async function () {
                        await this.token.approve(spender, new BN(1));
                    });

                    it('approves the requested amount and replaces the previous one', async function () {
                        await this.token.approve(spender, amount);

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount);
                    });
                });
            });
        });

        describe('when the spender is the zero address', function () {
            const amount = initialSupply;
            const spender = ZERO_ADDRESS;

            it('reverts', async function () {
                await shouldFail.reverting(this.token.approve(spender, amount));
            });
        });
    });

    describe('transferFrom', function () {
        const spender = recipient;

            describe('when the recipient is not the zero address or token contract', function () {
                const to = anotherAccount;

                describe('when the spender has enough approved balance', function () {
                    beforeEach(async function () {
                        await this.token.approve(spender, initialSupply, { from: initialHolder });
                    });

                    describe('when the initial holder has enough balance', function () {
                        const amount = initialSupply;

                        it('transfers the requested amount', async function () {
                            await this.token.transferFrom(initialHolder, to, amount, { from: spender });

                            (await this.token.balanceOf(initialHolder)).should.be.bignumber.equal('0');

                            (await this.token.balanceOf(to)).should.be.bignumber.equal(amount);
                        });

                        it('decreases the spender allowance', async function () {
                            await this.token.transferFrom(initialHolder, to, amount, { from: spender });

                            (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal('0');
                        });

                        it('emits a transfer event', async function () {
                            const { logs } = await this.token.transferFrom(initialHolder, to, amount, { from: spender });

                            expectEvent.inLogs(logs, 'Transfer', {
                                from: initialHolder,
                                to: to,
                                value: amount,
                            });
                        });

                        it('emits an approval event', async function () {
                            const { logs } = await this.token.transferFrom(initialHolder, to, amount, { from: spender });

                            expectEvent.inLogs(logs, 'Approval', {
                                owner: initialHolder,
                                spender: spender,
                                value: await this.token.allowance(initialHolder, spender),
                            });
                        });
                    });

                    describe('when the initial holder does not have enough balance', function () {
                        const amount = initialSupply.addn(1);

                        it('reverts', async function () {
                            await shouldFail.reverting(this.token.transferFrom(initialHolder, to, amount, { from: spender }));
                        });
                    });
                });

                describe('when the spender does not have enough approved balance', function () {
                    beforeEach(async function () {
                        await this.token.approve(spender, initialSupply.subn(1), { from: initialHolder });
                    });

                    describe('when the initial holder has enough balance', function () {
                        const amount = initialSupply;

                        it('reverts', async function () {
                            await shouldFail.reverting(this.token.transferFrom(initialHolder, to, amount, { from: spender }));
                        });
                    });

                    describe('when the initial holder does not have enough balance', function () {
                        const amount = initialSupply.addn(1);

                        it('reverts', async function () {
                            await shouldFail.reverting(this.token.transferFrom(initialHolder, to, amount, { from: spender }));
                        });
                    });
                });
            describe('when the recipient is the zero address', function () {
                const amount = initialSupply;
                const to = ZERO_ADDRESS;

                beforeEach(async function () {
                    await this.token.approve(spender, amount);
                });

                it('reverts', async function () {
                    await shouldFail.reverting(this.token.transferFrom(initialHolder, to, amount));
                });
            });
            describe('when the recipient is the token contract', function () {
                const amount = initialSupply;

                beforeEach(async function () {
                    await this.token.approve(spender, amount);
                })

                it('reverts', async function () {
                    await shouldFail.reverting(this.token.transferFrom(initialHolder, this.token.address, amount));
                })
            });
        });
    });

    describe('decreaseAllowance', function () {
        describe('when the spender is not the zero address', function () {
            const spender = recipient;

            function shouldDecreaseApproval (amount) {
                describe('when there was no approved amount before', function () {
                    it('reverts', async function () {
                        await shouldFail.reverting(this.token.decreaseAllowance(spender, amount, { from: initialHolder }));
                    });
                });

                describe('when the spender had an approved amount', function () {
                    const approvedAmount = amount;

                    beforeEach(async function () {
                        ({ logs: this.logs } = await this.token.approve(spender, approvedAmount, { from: initialHolder }));
                    });

                    it('emits an approval event', async function () {
                        const { logs } = await this.token.decreaseAllowance(spender, approvedAmount, { from: initialHolder });

                        expectEvent.inLogs(logs, 'Approval', {
                            owner: initialHolder,
                            spender: spender,
                            value: new BN(0),
                        });
                    });

                    it('decreases the spender allowance subtracting the requested amount', async function () {
                        await this.token.decreaseAllowance(spender, approvedAmount.subn(1), { from: initialHolder });

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal('1');
                    });

                    it('sets the allowance to zero when all allowance is removed', async function () {
                        await this.token.decreaseAllowance(spender, approvedAmount, { from: initialHolder });
                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal('0');
                    });

                    it('reverts when more than the full allowance is removed', async function () {
                        await shouldFail.reverting(
                            this.token.decreaseAllowance(spender, approvedAmount.addn(1), { from: initialHolder })
                        );
                    });
                });
            }

            describe('when the sender has enough balance', function () {
                const amount = initialSupply;

                shouldDecreaseApproval(amount);
            });

            describe('when the sender does not have enough balance', function () {
                const amount = initialSupply.addn(1);

                shouldDecreaseApproval(amount);
            });
        });

        describe('when the spender is the zero address', function () {
            const amount = initialSupply;
            const spender = ZERO_ADDRESS;

            it('reverts', async function () {
                await shouldFail.reverting(this.token.decreaseAllowance(spender, amount, { from: initialHolder }));
            });
        });
    });

    describe('increaseAllowance', function () {
        const amount = initialSupply;

        describe('when the spender is not the zero address', function () {
            const spender = recipient;

            describe('when the sender has enough balance', function () {
                it('emits an approval event', async function () {
                    const { logs } = await this.token.increaseAllowance(spender, amount, { from: initialHolder });

                    expectEvent.inLogs(logs, 'Approval', {
                        owner: initialHolder,
                        spender: spender,
                        value: amount,
                    });
                });

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        await this.token.increaseAllowance(spender, amount, { from: initialHolder });

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount);
                    });
                });

                describe('when the spender had an approved amount', function () {
                    beforeEach(async function () {
                        await this.token.approve(spender, new BN(1), { from: initialHolder });
                    });

                    it('increases the spender allowance adding the requested amount', async function () {
                        await this.token.increaseAllowance(spender, amount, { from: initialHolder });

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount.addn(1));
                    });
                });
            });

            describe('when the sender does not have enough balance', function () {
                const amount = initialSupply.addn(1);

                it('emits an approval event', async function () {
                    const { logs } = await this.token.increaseAllowance(spender, amount, { from: initialHolder });

                    expectEvent.inLogs(logs, 'Approval', {
                        owner: initialHolder,
                        spender: spender,
                        value: amount,
                    });
                });

                describe('when there was no approved amount before', function () {
                    it('approves the requested amount', async function () {
                        await this.token.increaseAllowance(spender, amount, { from: initialHolder });

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount);
                    });
                });

                describe('when the spender had an approved amount', function () {
                    beforeEach(async function () {
                        await this.token.approve(spender, new BN(1), { from: initialHolder });
                    });

                    it('increases the spender allowance adding the requested amount', async function () {
                        await this.token.increaseAllowance(spender, amount, { from: initialHolder });

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(amount.addn(1));
                    });

                    it('overwrites the previously approved amount', async function () {
                        await this.token.approve(spender, new BN(2), { from: initialHolder});

                        (await this.token.allowance(initialHolder, spender)).should.be.bignumber.equal(new BN(2));

                    });
                });
            });
        });


    });
});
