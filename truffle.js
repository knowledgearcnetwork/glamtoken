require('dotenv').config();

// Wallet Provider for Ropsten Test Net
const HDWalletProvider = require("truffle-hdwallet-provider");

/*
 * NB: since truffle-hdwallet-provider 0.0.5 you must wrap HDWallet providers in a
 * function when declaring them. Failure to do so will cause commands to hang. ex:
 * ```
 * mainnet: {
 *     provider: function() {
 *       return new HDWalletProvider(mnemonic, 'https://mainnet.infura.io/<infura-key>')
 *     },
 *     network_id: '1',
 *     gas: 4500000,
 *     gasPrice: 10000000000,
 *   },
 */

 module.exports = {
     // See <http://truffleframework.com/docs/advanced/configuration>
     // to customize your Truffle configuration!

     networks: {
         development: {
             host: "localhost",
             port: 9545,
             network_id:"*",
             "develop": {
                 accounts: 5,
                 defaultEtherBalance: 500,
                 blockTime: 3
             }
         },
         ropsten: {
             provider: new HDWalletProvider(process.env.MNEMONIC, process.env.ROPSTEN_URL, process.env.ROPSTEN_ACCOUNT_ID),
             network_id: "3",
             gas: 4500000,
             confirmations: 2,
             timeoutBlocks: 200,
             skipDryRun: true
        },
        rinkeby: {
            provider: new HDWalletProvider(process.env.MNEMONIC, process.env.RINKEBY_URL, process.env.RINKEBY_ACCOUNT_ID),
            network_id: "4",
            gas: 4500000,
            confirmations: 2,
            timeoutBlocks: 200,
            skipDryRun: true
       },
       mainnet: {
           provider: new HDWalletProvider(process.env.MNEMONIC, process.env.MAINNET_URL, process.env.MAINNET_ACCOUNT_ID),
           network_id: "1",
           confirmations: 6,
           timeoutBlocks: 200
      }
    }
};
